import * as express from 'express';

/**
 * Create/Update user
 * @param req 
 * @param res 
 */
export const createUpdateUser = (req: express.Request, res: express.Response) => {
  const { User, Address, Income } = req['models'];
  if ( !req.body.address ) {
    return res.status(400).send('Bad request missing address field!');
  }
  if ( !req.body.incomeGenerated ) {
    return res.status(400).send('Bad request missing incomeGenerated field!');
  }
  // Create user properties
  const createProperties = (id) =>
    Address.create({ ...req.body.address, owner: id })
      .then(() => {
        Income.create({
          value: req.body.incomeGenerated,
          owner: id
        }).then(() => {
          res.status(200).json({ id: id });
        })
      })
      .catch( err => {
        res.status(200).json(err);
      });
  
  User.findOne({
    where: {
      name: req.body.owner
    }
  }).then(user => {
    if ( !user ) {
      User.create({
        name: req.body.owner
      }).then(user => {
        createProperties(user.get('id'));
      })
    }
    createProperties(user.get('id'));
  })
};

/**
 * View user details
 * @param req 
 * @param res 
 */
export const viewUser = (req: express.Request, res: express.Response) => {
  const { User, Address, Income } = req['models'];
  if ( User ) {
    User.find({
      where: {
        id: req.params.id
      },
      include: [ Address, Income ]
    }).then( user => {
      if ( process.env.NODE_ENV === 'test' ) {
        return res.status(200).json(user);
      }
      res.render('view-user', { user });
    })
  }
}
/**
 * List all users
 * @param req 
 * @param res 
 */
export const userList = (req: express.Request, res: express.Response) => {
  const { User } = req['models'];
  if ( User ) {
    User.findAll().then( users => {
      if ( process.env.NODE_ENV === 'test' ) {
        return res.status(200).json(users);
      }
      res.render('users', { users })
    })
  }
}
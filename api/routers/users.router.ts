import * as process from 'process';
import * as express from 'express';
import { UserInstance } from '../models/interfaces/User';
import { createUpdateUser, viewUser, userList } from '../controllers/user.controller';

export const router: express.Router = express.Router();

/**
 * List all users
 */
router.get('/', userList);
router.get('/new', (req: express.Request, res: express.Response) => {
  res.render('forms/create-user');
});
/**
 * View owner by Id
 */
router.get('/view/:id', viewUser);
/**
 * Create new owner or add properties
 */
router.post('/add', createUpdateUser);
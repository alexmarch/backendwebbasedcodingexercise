import * as express from 'express';
/**
 * Static router
 */
export const router: express.Router = express.Router();

router.get('/', (req: express.Request, res: express.Response) => {
  res.redirect('users');
});
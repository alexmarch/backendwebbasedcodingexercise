import { Sequalize, DataTypes } from 'sequelize';
import { UserAttrs, UserInstance } from './interfaces/User';
import * as defineAddress from './Address';
import * as defineIncome from './Income';

export default ( sequelize: Sequalize, dataType: DataTypes): 
Sequalize.Model<UserAttrs, UserInstance> => {
  const Address =  defineAddress.default(sequelize, dataType);
  const Income = defineIncome.default(sequelize, dataType);

  const User = sequelize.define('User', {
    id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
    name: {
      type: DataTypes.STRING,
      allowNull: false
    }
  },
  {
    timestamps: false,
    tableName: 'users'
  });
  User.hasMany(Address, { foreignKey: 'owner' });
  User.hasOne(Income, { foreignKey: 'owner' })
  return User;
}

import { Instance } from 'sequelize';

export interface AddressAttrs {
  name?: string;
  line1?: string;
  line2?: string;
  line3: string;
  line4: string;
  postCode?: string;
  city?: string;
  country?: string;
  owner?: any;
}

export interface AddressInstance extends Instance<AddressAttrs> {
  dataValue: AddressAttrs
}
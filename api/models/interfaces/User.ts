import { Instance } from 'sequelize';

export interface UserAttrs {
  name?: string;
}

export interface UserInstance extends Instance<UserAttrs> {
  dataValue: UserAttrs
}
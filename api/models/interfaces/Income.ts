import { Instance } from 'sequelize';

export interface IncomeAttrs {
  value?: number;
  owner?: any
}

export interface IncomeInstance extends Instance<IncomeAttrs> {
  dataValue: IncomeAttrs
}
import * as fs from 'fs';
import * as path from 'path';
import { Sequelize } from 'sequelize';


const basename  = path.basename(module.filename);
const env       = process.env.NODE_ENV || 'development';
const config    = require(__dirname + '/../../config/config.json')[env];

let models = {};

export interface Database {
  sequelize: Sequelize,
  models: any
}

let sequelizeInstance: Sequelize;

if (config.use_env_variable) {
  sequelizeInstance = new Sequelize(process.env[config.use_env_variable]);
} else {
  sequelizeInstance = new Sequelize(config.database, config.username, config.password, config);
}

fs
  .readdirSync(__dirname)
  .filter( file => {
    return (file.indexOf('.') !== 0) && (file !== basename) 
    && ( file !== 'interfaces' ) 
    && ( file.indexOf('_') !== 0 ) 
    && (file.slice(-3) === '.ts');
  })
  .forEach( file => {
    var model = sequelizeInstance['import'](path.join(__dirname, file));
    models[model.name] = model;
  });

  Object.keys(models).forEach( modelName => {
    if (models[modelName].associate) {
      models[modelName].associate(models);
    }
  });

  export var database: Database = {
    models: models,
    sequelize: sequelizeInstance
  };


import { Sequalize, DataTypes } from 'sequelize';
import { AddressAttrs, AddressInstance } from './interfaces/Address'

export default ( sequelize: Sequalize, dataType: DataTypes):Sequalize.Model<AddressAttrs, AddressInstance> => 
  sequelize.define('Address',{
    id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
    line1: {
      type: DataTypes.STRING,
      allowNull: false,
      notEmpty: true, 
    },
    line2: {
      type: DataTypes.STRING,
      allowNull: true
    },
    line3: {
      type: DataTypes.STRING,
      allowNull: true
    },
    line4: {
      type: DataTypes.STRING,
      allowNull: false,
      notEmpty: true
    },
    postCode: {
      type: DataTypes.STRING,
      allowNull: false,
      notEmpty: true
    },
    city: {
      type: DataTypes.STRING,
      allowNull: false,
      notEmpty: true
    },
    country: {
      type: DataTypes.STRING,
      allowNull: false,
      notEmpty: true
    },
    owner: {
          type: DataTypes.INTEGER,
          references: { 
            model: 'Users',
            key: 'id'
          }
    }
  },{
    timestamps: false,
    tableName: 'addresses'
  });

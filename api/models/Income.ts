import { Sequalize, DataTypes } from 'sequelize';
import { UserAttrs, UserInstance } from './interfaces/User'

export default ( sequelize: Sequalize, dataType: DataTypes): 
Sequalize.Model<UserAttrs, UserInstance> => 
  sequelize.define('Income',{
    id: {
			type: DataTypes.BIGINT,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
    value: {
      type: DataTypes.DOUBLE,
      allowNull: false
    },
     owner: {
          type: DataTypes.INTEGER,
          references: { 
            model: 'Users',
            key: 'id'
          }
    }
  },{
    timestamps: false,
    tableName: 'incomes'
  });

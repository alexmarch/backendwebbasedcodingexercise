# BackendWebBasedCodingExercise
### Typescrypt/NodeJS/Express/Sequalize.

### Node/Npm versions:
node v6.11.2,
npm v3.10.10.

## How to Install/Run:
You can use any package manager npm/yarn.
```bash
git clone git@bitbucket.org:alexmarch/backendwebbasedcodingexercise.git
cd backendwebbasedcodingexercise
yarn install # Install all mudules
yarn start # Start server
```
Then you can go to http://localhost:3000 and check all application.

## Docker
You can install docker from https://docs.docker.com/engine/installation/#desktop.
## Database: 
PostgresSQL script you can use my docker compose file (docker-compose.yml) and run next command:
```bash
docker-compose up # Install images and up container
```
OR
```bash
docker-compose up -d # Run in background 
# or
yarn run postgres
```
Also you can stop postgres docker container by the following command:
```bash
yarn run postgres:stop
```
## Migrations
First you need to setup configuration for postgres provider, you need to open config/config.json config file and change default options.
### Run migrations:
```bash
yarn run migrate
# or
yarn run migrate:undo # Undo all migrations
```
## Seeders
You can insert default data by seeders.
### Run seeders:
```bash
  yarn run seed
```
## Tests
You can run testing.
```bash
  yarn test
```

function createUserSubmit(event) {
  event.stopPropagation();
  event.preventDefault();
  var result = {
    address: {}
  };
  var $form = $('#createUserForm');
  var arrayData = $form.serializeArray();
  $.map(arrayData, function(item){
    if( item.name === 'country' ) {
      result['address']['country'] = $('option[value="'+item.value+'"]').text(); 
    } else {
      if(item.name !== 'owner' && item.name !== 'incomeGenerated'){
        result['address'][item.name] = item.value;
      } else {
        result[item.name] = item.value;
      }
    }
  });

  fetch('/users/add', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(result)
  })
  .then(function(resp){
    return resp.json();
   
  })
  .then( function(json){
    console.log('resp', json);
    if ( json.id ) {
      location.href = 'view/' + json.id;
    }
  })
  .catch(function(error){
    console.log('Error', error);
  })
}
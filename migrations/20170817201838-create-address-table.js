'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
      return queryInterface.createTable('addresses', { 
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },
        "line1": {
          type: Sequelize.STRING,
          allowNull: false
        },
        "line2": {
          type: Sequelize.STRING,
          allowNull: true
        },
        "line3": {
          type: Sequelize.STRING,
          allowNull: true
        },
        "line4": {
          type: Sequelize.STRING,
          allowNull: false
        },
        "postCode": {
          type: Sequelize.STRING,
          allowNull: false
        },
        "city": {
          type: Sequelize.STRING,
          allowNull: false
        },
        "country": {
          type: Sequelize.STRING,
          allowNull: false
        },
        owner: {
          type: Sequelize.INTEGER,
          references: { 
            model: 'users',
            key: 'id'
          }
        }
      });
  },

  down: function (queryInterface, Sequelize) {
      return queryInterface.dropTable('addresses');
  }
};

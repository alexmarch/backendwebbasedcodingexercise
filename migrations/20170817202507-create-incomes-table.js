'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
      return queryInterface.createTable('incomes', 
      { 
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },
        owner: {
          type: Sequelize.INTEGER,
          references: { 
            model: 'users',
            key: 'id'
          }
        },
        value: {
          type: Sequelize.DOUBLE
        }
      });
  },

  down: function (queryInterface, Sequelize) {
      return queryInterface.dropTable('incomes');
  }
};

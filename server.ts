import * as http from 'http';
import * as express from 'express';
import { Express, Router } from 'express';
import * as bodyParser from 'body-parser';
import * as cors from 'cors';
import * as propertiesRoute from './api/routers/properties.router';
import * as staticRoute from './api/routers/static.router';
import * as userRoute from './api/routers/users.router';
import * as path from 'path';
import { database } from './api/models';
import * as sassMiddleware from 'node-sass-middleware';

// Init express application
const app: Express = express();
const router: Router = Router();

// Normalize port depending NODE_ENV
const normalizePort = () => {
  if ( process.env.NODE_ENV === 'test' ) {
    return 3001;
  }
  return process.env.PORT || 3000;
}

// Bootstrap express application
app.set('port', normalizePort()); // Configure app port
app.set('view engine', 'jade');

app.locals.models = database.models;

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use('/assets', express.static(path.join(__dirname, 'assets')));
app.use(
  sassMiddleware({
        src: __dirname + '/assets/css', // where the sass files are 
        dest: __dirname + '/assets/css', // where css should go
        outputStyle: 'compressed',
        debug: true,
        prefix: '/assets/css'
    })
);

app.use((req: express.Request, res: express.Response, next?: any) => {
  req['models'] = app.locals.models;
  next();
})
app.use('/properties', propertiesRoute.router);
app.use('/users', userRoute.router);
app.use('/', staticRoute.router); // Init routings

// Create server 
export const server = http.createServer(app);

// List server address/port 
server.listen(app.get('port'), () => {
  let { port } = server.address();
  console.log( `Server start at : http://localhost:${port}` );
})
.on("error", err => console.log(err));

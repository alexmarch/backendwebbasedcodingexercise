'use strict';

const defaultData = require('../data/exerciseData.json');

module.exports = {
  up: function (queryInterface, Sequelize) {
      let sequelize = queryInterface.sequelize;
      var userIndex = 1;
      return new Promise( (resolve) => {
        sequelize.query(
          'SELECT last_value FROM users_id_seq',
          { type: sequelize.QueryTypes.SELECT }
          ).then( users => {
            if ( !users || users.length === 0 ) {
              userIndex = 1;
            } else {
              let lastValue = parseInt(users[0].last_value);
              if ( lastValue === 1) {
                userIndex = lastValue;
              } else {
                userIndex = ( lastValue  + 1) ;
              }
            }
          
            var promises = [];
            for ( let item of defaultData ) {
              item.address.owner = userIndex;
              promises.push(
                new Promise( resolve => {
                    queryInterface.bulkInsert('users', [{ name: item.owner }], {})
                      .then( () => {
                          console.log('add', item.address);
                          queryInterface.bulkInsert('addresses', [item.address], {})
                            .then( () => {
                              queryInterface.bulkInsert('incomes', [{
                                owner: item.address.owner,
                                value: item.incomeGenerated
                              }], {});
                              resolve();
                            })
                      })
                  }
                ))
              userIndex++;
            }
            resolve(Promise.all(promises));
          })
        }
      )
  },
  down: function (queryInterface, Sequelize) {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('Person', null, {});
    */
  }
};

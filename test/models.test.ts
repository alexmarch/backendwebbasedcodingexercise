import * as process from 'process';
import { database } from '../api/models';
import { should, expect } from 'chai';


describe('Models', () => {
  describe('Models Loader', () => {
    it ('it should have object with models and sequelize instance', (done) => {
        expect(database).property('models').is.not.eqls({});
        expect(database).property('sequelize').is.not.eqls(null);
        done();
    });
  });
});
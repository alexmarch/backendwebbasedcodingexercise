import * as console from 'console';
import * as process from 'process';
import { should, use, request, expect } from 'chai';
import { server } from '../server';

const chaiHttp = require('chai-http');

should();
use( chaiHttp );

describe('Users', () => {
  // Test GET all users
  describe('/GET Users', () => {
    it('it should GET all users', done => {
      request(server)
        .get('/users')
        .end( (err, res) => {
          res.should.to.have.status(200);
          res.body.should.be.a('array');
          res.body.should.not.be.eql(null);
          
          done();
        });
    });
  });
  // View user by Id
  describe('/GET View user', () => {
    it('it should GET user by id', done => {
      let userId = 1;
      request(server)
        .get(`/users/view/${userId}`)
        .end( (err, res) => {
          res.should.to.have.status(200);
          res.body.should.not.be.eql(null);
          res.body.should.have.property('Addresses');
          res.body.should.have.property('Income');
          done();
        });
    });
  });
  // Add new user with data
  describe('/POST Add new user', () => {
    it('it should POST create new user', done => {
      const user = {
        "owner": "carlos1",
        "address": {
            "line1": "Flat 5",
            "line4": "7 Westbourne Terrace",
            "postCode": "W2 3UL",
            "city": "London",
            "country": "U.K."
        },
        "incomeGenerated": 2000.34
      };
      request(server)
        .post('/users/add')
        .send(user)
        .end( (err, res) => {
          res.should.to.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('id');
          done();
        });
    });
    // Test validation messages 
    it('it should return validation error message', done => {
      const user = {
        "owner": "carlos",
        "address": {
            "line4": "7 Westbourne Terrace",
            "postCode": "W2 3UL",
            "city": "London",
            "country": "U.K."
        },
        "incomeGenerated": 2000.34
      };
      request(server)
      .post('/users/add')
      .send(user)
      .end( (err, res) => {
        res.should.to.have.status(200);
        res.body.should.have.property('errors');
        expect(res.body.errors).to.not.be.empty;
        expect(res.body.errors[0].message).to.have.string('line1 cannot be null')
        done();
      });
    });
  });
});